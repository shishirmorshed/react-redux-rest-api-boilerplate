# React Redux REST API Boilerplate



### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ git clone https://gitlab.com/shishirmorshed/react-redux-rest-api-boilerplate.git
$ cd [folderName]
$ npm install
$ npm start
```

For production environments...

```sh
$ npm install
$ npm run build
```

### Build & Host
**Hosted App URL** - [Add your published app URL here]

**Build Package** - https://gitlab.com/shishirmorshed/react-redux-rest-api-boilerplate/-/raw/master/dist.zip
